# CircleCI configuration

There is a repository at which is configured to build automatically via
CircleCI. The configuration uses a secret gist to pull the signing keys, but
other than that, it's pretty standard stuff.

The repository is at: https://github.com/ressu/Gadgetbridge
One built copy is at: https://transfer.sh/R2P05/app-release-via-circleci.apk

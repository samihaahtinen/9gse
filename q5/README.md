# Scalability puzzle

There are at least two solutions that come to mind when considering such a high
volume of queries. Lets start off with the simplified solution.

Since the question doesn't mention any actual solutions, I'm sticking with
theory only.

## Solution 1: Plain SQL replication

Since we are dealing with read only data and it's pretty obvious when the user
is actually attempting to write to the database. I would propose a master-slave
solution for this problem.

I would set up the application so that all read-only queries are sent to a pool
of read-only slaves. The database master would be used only for writing data.
This way we would be able to increase the processing power by simply adding
more slaves.

[SQL-master/slave drawing](SQL-master-slave.png)

The setup would need to have a load balancer in between of the slave hosts in
order to avoid overloading a single slave.

The upside of this solution is that the application itself can be modified
(assuming it doesn't already follow this schema) quite easily to distribute
read and write queries to different hosts, by simply having multiple
connections to the databases.

Note that this solution doesn't try to solve high availability, multi master
solutions would be a good start in making this solution more resilient

In order to satisfy the required query speeds, the service would need at least
25 slave servers just for this cause. And this doesn't of course account for
possible other overhead from queries outside of the scope of the question.

Another possible bottleneck would be the master server. Since all of the writes
caused by the application are directed at the master server, the amount of
queries might exceed the named 2000 qps. Multi-master solutions might help in
that case, but without proper analysis of the ratio of reads and writes, it's
difficult to estimate the allocation need for the master pool.

## Solution 2: Reduce the number of queries

While the question draws attention to the SQL queries, the scoring subsystem
could consist of a caching component in addition to the SQL servers.

The multi slave solution would need to be implemented in this case as well, but
the goal is not to increase the query rate, but rather reduce the number of
queries needed for a page load.

Instead of loading the 20 datapoints on each page query, the data could be
cached in a separate service (like Redis or memcache). Assuming that the global
scores were cached, the SQL servers would have to serve 50% less datapoints per
pageload. Since the global datapoints are just that, global, the cache could be
invalidated when ever a change is made to the affecting data and that way the
users themselves would still get just as fresh data, as with plain SQL queries.

[SQL cluster with cache](SQL-cache.png)

This solution requires modifications to the underlaying software. Since now the
software needs to be aware of cache data related to the queries being made.
Every time data is being updated, some cache will need invalidating.

This also brings out a caveat in the plan. If certain score starts to get a lot
of updates and pageviews, the queries would be missing cache quite often. This
makes predicting database load difficult, since viewer trends might change in
unexpected ways.

The solution would be to make the cache auto expire after certain time for high
load items (like the frontpage). Instead of invalidating cache items each time
the actual data changes, leave a small grace period for the data and catch up
on more predictable intervals. This would cause some of the scores to be out of
date for a few moments, but still fresh enough to count.

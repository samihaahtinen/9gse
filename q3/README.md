# Slack bot for deployment

This is a simplified Slack deployment bot, it will attempt to deploy the named
commit to the named app on the named stage.

It's a fairly straightforward bot. The bot can be run with the environment
variables API_HOST (to set the target host) and SLACK_API_TOKEN to configure
the slack authentication.

## Implementation considerations

The bot is simple and is accompanied by a simplified (random) version of the
specified API. Due to the implementation of the mock API, the bot relies on
internal state for deployment status instead of querying the API for the real
status. If this bot is run against a real API, it will miss the deployments
started from other sources.

The bot also checks for the target environment to deploy to (staging), but
doesn't support different hosts for different environments. I'm assuming that
there are multiple hosts, since the example api doesn't specify target stage
for the deployment call.

#!/usr/bin/env ruby

require 'rubygems'
require 'sinatra'
require 'json'

configure do
  @@hashes = {}
end

# Return a dummy deployment id
post '/deploy/:project/:commit' do |project, commit|
  content_type :json
  newid = "#{project}_#{(rand * 1_000_000).to_i}"
  @@hashes[newid] = { project: project, commit: commit }
  { 'deploy-id' => newid }.to_json
end

# Randomly reply with deployed. We want about 10% of requests to be success to
# have a reasonable window for a commit to "complete"

get '/:deploy_id/status' do |deploy_id|
  pass unless @@hashes.key?deploy_id
  content_type :json
  status = (rand * 10 > 9)
  data = @@hashes[deploy_id]
  @@hashes.delete deploy_id if status
  { 'deploy-status' => status ? 1 : 0 }.merge(data).to_json
end

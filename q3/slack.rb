#!/usr/bin/env ruby

require 'rubygems'
require 'slack-ruby-bot'
require 'http'
require 'timers'

# Stop all the silliness..
SlackRubyBot.configure do |config|
  config.send_gifs = false
end

# simple bot to handle deployment requests
class DeployBot < SlackRubyBot::Bot
  match(/^deploy (?<app>\w*) commit (?<commit>[0-9a-f]*) to (?<target>\w*)$/) \
    do |client, data, match|
    # simply ignore unkown environments.
    next unless valid_env(match[:target])
    if !running?match[:target], match[:app]
      start_deploy(match[:app],
                   match[:commit],
                   match[:target],
                   client,
                   data.channel)
      client.say(
        text: "deploying #{match[:app]} commit #{match[:commit]} "\
        "to #{match[:target]}",
        channel: data.channel
      )
    else
      client.say(
        text: "Someone else is deploying to #{match[:app]} #{match[:target]}",
        channel: data.channel
      )
    end
  end

  class << self
    KNOWN_ENVS = [:staging].freeze

    def valid_env(key)
      KNOWN_ENVS.include?key.to_sym
    end

    # Idea here is to fetch the API address from $API_HOST, or default to
    # localhost:4567 which is the development environment
    def base_url
      "http://#{ENV.fetch 'API_HOST', 'localhost:4567'}"
    end

    def deploy_url(app, commit)
      "#{base_url}/deploy/#{app}/#{commit}"
    end

    def status_url(deploy_id)
      "#{base_url}/#{deploy_id}/status"
    end

    # Helpers to keep track of running deployments
    def ongoing
      @@ongoing ||= Hash.new({})
    end

    def running?(target, app)
      ongoing[target].fetch(app, false)
    end

    # Start deployment
    def start_deploy(app, commit, target, client, channel)
      r = HTTP.post deploy_url(app, commit), form: {}
      i = JSON.parse(r.body)
      ongoing[target][app] = true
      poll_thread(app, target, i['deploy-id'], client, channel)
      true
    end

    # Check current status
    def fetch_status(deploy_id)
      r = HTTP.get status_url(deploy_id)
      JSON.parse(r.body)
    end

    # Create a new thread to wait for a commit to finish
    def poll_thread(app, target, deploy_id, client, channel)
      Thread.new do
        r = { 'deploy-status' => 0 }
        while r['deploy-status'] == 0
          sleep(5)
          r = fetch_status(deploy_id)
        end

        client.say(
          text: "deployed #{r['project']} commit #{r['commit']} to #{target}",
          channel: channel
        )
        ongoing[target].delete app
      end
    end

  end
end

DeployBot.run

#!/usr/bin/env ruby

# Do the gem loader dance, just in case the verification environment is set up
# without bundler.
begin
  require 'geoip'
  require 'xz'
rescue LoadError
  require 'rubygems'
  require 'geoip'
  require 'xz'
end

require 'set'

# Target filename
filename = '/vagrant/logs/2015081616.log.xz'
# For local testing
# filename = '2015081616.log.xz'

@geoip = GeoIP.new('GeoIP.dat')
@countries = Hash.new(0)
@logregex = %r{\(\/cacheable\)$}
@addresses = Set.new

# Pluck out IP addresses from the logfile
XZ::StreamReader.open(filename) do |f|
  while (l = f.gets)
    next unless l =~ @logregex
    @addresses << l.split('][')[1]
  end
end

# Resolve unique IP addresses to countries
@addresses.each do |address|
  @countries[@geoip.country(address).country_name] += 1
end

# Sort the Hash by hit count
sorted = @countries.sort_by { |_, count| count }

# Print out 5 entries from sorted list
5.times do
  x = sorted.pop
  break if x.nil? # Quick failsafe if we have less than 5 entries
  puts "#{x[0]} (#{x[1]})"
end

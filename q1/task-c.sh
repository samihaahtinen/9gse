#!/bin/sh

# Make life easier with a variables.

# Target filename
FILENAME=/vagrant/logs/2015081616.log.xz
# For local testing
# FILENAME=2015081616.log.xz

xz -dc $FILENAME | \
  ruby -ne 'BEGIN{@t=0.0; @r=/Request took ([0-9.]*) ms/}; @t += @r.match($_)[1].to_f; END{puts @t/$.}'

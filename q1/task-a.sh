#!/bin/sh

# Make life easier with a variables.

# Target filename
FILENAME=/vagrant/logs/2015081616.log.xz
# For local testing
# FILENAME=2015081616.log.xz

# Target match, since we want a relatively simple match, lets use a plain and
# simple grep
TOMATCH='2015-08-16T16:1[0-2]:'
# For local testing
# TOMATCH='2015-08-16T16:04:4[2-3]'

xz -dc $FILENAME | grep -c $TOMATCH

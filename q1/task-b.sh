#!/bin/sh

# Make life easier with a variables.

# Target filename
FILENAME=/vagrant/logs/2015081616.log.xz
# For local testing
# FILENAME=2015081616.log.xz

xz -dc $FILENAME | awk -F '[][]' '{print $4}' \
  | sort | uniq -c | sort -n | tail -n 5

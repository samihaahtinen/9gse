# Log parsing

These are scripts to parse logfiles in designated format.

## Tools and libraries used

The tools for tasks a-c are pretty simple shell scripts. They make use of
standard tools available in most Linux environments.

The D script is a ruby script that requires some additional tools. We make use
of the [GeoIP library](https://github.com/cjheath/geoip) and the GeoLiteCountry
databse downloaded from maxmind. (Links available on the GeoIP Library
homepage)

## environment setup

The dependencies are listed in Gemfile, so required ruby packages can be
installed with bundler.

require './cluster'
require 'uuid'

startup_nodes = [
  { host: 'master', port: 6379 }
]

r = RedisCluster.new(startup_nodes, 32, timeout: 0.1)

# Generate 1000 random items. Lets use UUID as the key to make sure they are
# unique (and random))
(0..1000).each do |x|
  # Catch possible errors
  begin
    puts "#{x} items added" if x % 100 == 0
    r.set("item:#{UUID.generate}", Random.rand * 1000)
  rescue => e
    puts "error #{e}"
  end
end

#!/bin/bash

d_run() {
  docker-compose run --rm $@
}

self_ip() {
  d_run $1 hostname -i
}

master_ip() {
  d_run master getent hosts master | awk '{print $1; exit}'
}

redis_cli() {
  d_run master redis-cli -h master $@
}

known_slaves() {
  redis_cli cluster nodes | awk '/slave/ {print $2}' | cut -d: -f1 | sort
}

running_slaves() {
  d_run master getent hosts slave | awk '{print $1}' | sort
}

unknown_slaves() {
  running_slaves > running
  known_slaves > known
  comm -23 running known
  rm -r running known
}

add_slaves() {
  d_run trib add-node --slave $1:6379 $(master_ip):6379
}

# Scale to number of hosts. Assume 3..
SLAVES=3
if [ -n "$1" ]; then
  SLAVES=$1
fi

docker-compose scale slave=$SLAVES

for i in $(unknown_slaves); do
  add_slaves $i
done

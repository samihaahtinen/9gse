#!/bin/bash

d_run() {
  docker-compose run --rm $@
}

d_exec() {
  docker-compose exec $@
}

redis_cli() {
  d_run master redis-cli -h master $@
}

report_failed() {
  redis_cli cluster nodes | grep ,fail
}

check_cluster() {
  d_exec sentinel redis-cli -p 26379 info | grep -E ^master
}

SLEEPY_SLEEP=5

if [ -n "$1" ]; then
  SLEEPY_SLEEP=$1
fi

while true; do
  echo "--"
  check_cluster
  report_failed
  sleep $SLEEPY_SLEEP
done

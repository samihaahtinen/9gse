#!/bin/bash

d_run() {
  docker-compose run --rm $@
}

master_ips() {
  d_run master getent hosts master | awk '{print $1}'
}

n=0
# We will use a quorum of 1 since there will be just one sentinel
echo "#Configuration for sentinel, generated from running state at $(date)" > sentinel.conf
for i in $(master_ips); do
  n=$((n + 1))
  echo "sentinel monitor master$n $i 6379 1" >> sentinel.conf
done

docker-compose up -d sentinel

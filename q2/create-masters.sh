#!/bin/bash

d_run() {
  docker-compose run --rm $@
}

running_masters() {
  d_run master getent hosts master | awk '{print $1}' | sort
}

create_cluster() {
  d_run trib create $(for i in $(running_masters); do echo -n "$i:6379 "; done)
}

# Scale to number of hosts. Assume 3..
MASTERS=3
if [ -n "$1" ]; then
  MASTERS=$1
fi

docker-compose up -d master

docker-compose scale master=$MASTERS

create_cluster

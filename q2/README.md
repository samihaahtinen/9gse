# Semi-Unattended installation of Redis

This installation is based on docker, it is built on the premise that the
application that is using docker as an underlying layer.

The reasoning why I picked docker for the packaging is that with docker I don't
need to worry about HA setup since docker already provides an single point of
contact for applications. This does imply that the application has to be able
to resolve a name in to an IP address for the initial point of contact.

Since redis doesn't support name resolution for setup there are certain tricks
used to fetch a valid address.

# Environment setup
The setup depends on 2 components.
[Docker-engine](https://docs.docker.com/linux/step_one/) and Docker-compose,
which can be installed with pip: `pip install -r requirements.txt`

Other parts of the setup are automatically downloaded from Docker Hub

## Design plan
The idea was to use an unaltered redis image from docker hub. Due to the way
how Docker mounts configuration inside the containers, I ended up creating a
small container with the redis configuration inside.

Docker itself was picked because we can make use of it in a distributed and a
single host environment. Docker swarm supports multiple hosts and the redis
instances could easily be distributed to multiple hosts.

Setup is divided in to 3 sections. Master, Slave and Sentinel. The current
configuration is built with a single sentinel, instead of the recommended 3
sentinels to simplify the demonstration.

For the purpose of the demonstration, I decided not to hard bind the slaves to
any of the masters, but rather leave the designation to redis itself. The
default selection method seems rather sane, so there was no reason to change
it.

## Caveats
Downsizing the cluster is not supported by these scripts and care should be
taken when create-slaves.sh is invoked. Downsizing would require the slave to
be verified to be an actual slave, taken offline and then deleted.

## Initial setup
The initial setup asks questions about the initial cluster setup. I left these
to be manual, the answers could be provided from an answer file if needed.

The setup is 3 step one.

```
./create-masters.sh
./start-sentinel.sh
./create-slaves.sh
```

The first command asks for input, others are fully automatic. Both
create-masters and create-slaves accept a parameter to indicated the desired
number of instances.

This is used to adjust the number of slaves if the environment needs to be
scaled up. The default number for both is 3. The environment can easily be
adjusted to 12 slaves by simply running:

```
./create-slaves.sh 12
```

Care should be taken not to use a smaller amount of slaves than currently
exists in the cluster.

We can add some test data to the database by running:

```
./add-items.sh 12
```

# Failover setup
Cluster state management is handled by sentinel. The default configuration
consists of a single sentinel.

The cluster recovers from failures to master. We can monitor the cluster state
with the script `cluster-status.sh`

We start off by having a working cluster:

    master0:name=master3,status=ok,address=172.19.0.3:6379,slaves=1,sentinels=1
    master1:name=master1,status=ok,address=172.19.0.2:6379,slaves=1,sentinels=1
    master2:name=master2,status=ok,address=172.19.0.4:6379,slaves=1,sentinels=1

Lets kill off a master. First we need to find out the names of the masters:

```
docker-compose ps
```

        Name                   Command               State    Ports
    -----------------------------------------------------------------
    q2_master_1     docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_master_2     docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_master_3     docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_sentinel_1   docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_slave_1      docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_slave_2      docker-entrypoint.sh redis ...   Up      6379/tcp
    q2_slave_3      docker-entrypoint.sh redis ...   Up      6379/tcp

We will kill master 1:

```
docker kill q2_master_1
```

The cluster state is now failed:

    master0:name=master3,status=ok,address=172.19.0.3:6379,slaves=1,sentinels=1
    master1:name=master1,status=ok,address=172.19.0.2:6379,slaves=1,sentinels=1
    master2:name=master2,status=ok,address=172.19.0.4:6379,slaves=1,sentinels=1
    19c5286376ab4a4b0e0c91ffc335aadfe2c16643 172.19.0.2:6379 master,fail - 1464637005986 1464637005085 1 connected

The upper part of the message is from the sentinel. It takes a moment for it to
catch up. Latter part is the node status, which shows that one of the masters
has failed

After a moment, sentinel decides that the master is now down.

    master0:name=master3,status=ok,address=172.19.0.3:6379,slaves=1,sentinels=1
    master1:name=master1,status=odown,address=172.19.0.2:6379,slaves=1,sentinels=1
    master2:name=master2,status=ok,address=172.19.0.4:6379,slaves=1,sentinels=1
    19c5286376ab4a4b0e0c91ffc335aadfe2c16643 172.19.0.2:6379 master,fail - 1464637005986 1464637005085 1 connected

And repairs the cluster:

    master0:name=master3,status=ok,address=172.19.0.3:6379,slaves=1,sentinels=1
    master1:name=master1,status=ok,address=172.19.0.6:6379,slaves=1,sentinels=1
    master2:name=master2,status=ok,address=172.19.0.4:6379,slaves=1,sentinels=1
    19c5286376ab4a4b0e0c91ffc335aadfe2c16643 172.19.0.2:6379 master,fail - 1464637005986 1464637005085 1 connected

The old master is still listed as failed. Even though the cluster has
recovered, there is a node missing. Lets bring back the master that failed.

```
docker kill q2_master_1
```

The missing node is no longer listed as failed.

    master0:name=master3,status=ok,address=172.19.0.3:6379,slaves=1,sentinels=1
    master1:name=master1,status=ok,address=172.19.0.6:6379,slaves=1,sentinels=1
    master2:name=master2,status=ok,address=172.19.0.4:6379,slaves=1,sentinels=1

There is one important thing to note after this procedure. The setup doesn't
try to reassign the restored master as a master server. That should be done
manually once the reason for the failure has been diagnosed. It is important
that the nodes that are started as master in the Docker network are actually
masters in the long run, since hosts are connecting to the nodes with the name
master. If the statuses are left as they are, sentinel might try to use the
wrong nodes etc.

But for the purposes of the demonstration the node switchover is left to the
operator.

# High Availability
The task called for high availability, and suggested HAProxy as a medium for
connectivity. I opted for docker native name resolution for the initial
connection. While it isn't as versatile as HAProxy, it offers a single point of
contact for the application. The name "master" is always pointing to the master
instances.

The setup is similar to a DNS based load balancing or multihoming.

# Design limitations
It is important to note that this setup is pretty docker centric. We make an
assumption that the service that will be using Redis is a dockerized
application. Special care is taken to ensure ease of use inside a docker
network, but deploying a Redis instance like this for a non-dockerized
application is not reasonable. Since redis uses IP addresses and direct
conenctions, network routing would become the first issue in accessing the
dockerized services.
